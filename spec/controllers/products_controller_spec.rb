require 'rails_helper'

describe ProductsController do
  # CREATE
  describe 'POST create' do

    describe 'with valid attributes' do
      it 'creates a new Product' do
        expect {
          xhr :post, :create, {product: FactoryGirl.attributes_for(:product)}
        }.to change(Product, :count).by(1)
      end

      it 'assigns a newly created product as @product' do
        xhr :post, :create, {product: FactoryGirl.attributes_for(:product)}
        expect(assigns(:product)).to be_a(Product)
        expect(assigns(:product)).to be_persisted
      end

      it 'returns a true @result' do
        xhr :post, :create, {product: FactoryGirl.attributes_for(:product)}
        expect(assigns(:result)).to eq(true)
      end
    end

    describe 'with invalid attributes' do
      it 'assigns a newly created but unsaved product as @product' do
        expect {
          xhr :post, :create, {product: FactoryGirl.attributes_for(:product, price: -1)}
        }.to_not change(Product, :count)
      end

      it 'returns a false @result' do
        xhr :post, :create, {product: FactoryGirl.attributes_for(:product, price: -1)}
        expect(assigns(:result)).to eq(false)
      end

      it 'assigns @product with errors' do
        xhr :post, :create, {product: FactoryGirl.attributes_for(:product, price: -1)}
        expect(assigns(:product).errors.empty?).to eq(false)
      end
    end
  end

  describe 'GET index' do
    it 'assigns all products as @products, sorted in descending order of date_created' do
      product1 = FactoryGirl.create(:product)
      product2 = FactoryGirl.create(:product)
      get :index
      expect(assigns(:products)).to eq([product2, product1])
    end
  end

  # READ
  describe 'GET show' do
    before { @product = FactoryGirl.create(:product) }

    it 'assigns the requested product as @product' do
      xhr :get, :show, {:id => @product.id}
      expect(assigns(:product)).to eq(@product)
    end

  end

  describe 'GET new' do
    it 'assigns a new product as @product' do
      xhr :get, :new
      expect(assigns(:product)).to be_a_new(Product)
    end
  end

  describe 'GET edit' do
    it 'assigns the requested product as @product' do
      product = FactoryGirl.create(:product)
      xhr :get, :edit, {:id => product.id}
      expect(assigns(:product)).to eq(product)
    end
  end

  # UPDATE
  describe 'PUT update' do
    before :each do
      @product = FactoryGirl.create(:product) # Creates and saves valid object
    end

    describe 'with valid attributes' do

      it 'assigns the requested product as @product' do
        xhr :put, :update, id: @product.id , product: FactoryGirl.attributes_for(:product)
        expect(assigns(:product)).to eq(@product)
      end

      it 'changes @product attributes' do
        xhr :put, :update, id: @product.id ,
          product: FactoryGirl.attributes_for(:product, name: 'Fresh Sotong',
          description: 'Going cheap!', price: 0.1)
        @product.reload
        expect(@product.name).to eq('Fresh Sotong')
        expect(@product.description).to eq('Going cheap!')
        expect(@product.price).to eq(0.1)
      end

      it 'redirects to the product' do
        xhr :put, :update, id: @product.id , product: FactoryGirl.attributes_for(:product)
        expect(response).to redirect_to(@product)
      end
    end

    describe 'with invalid params' do

      it 'does not save the invalid product' do

        xhr :put, :update, id: @product.id ,
          product: FactoryGirl.attributes_for(:product, price: -1)
        @product.reload
        expect(@product.price).to_not eq(-1)
      end

      it 'returns a false @result' do
        xhr :put, :update, id: @product.id ,
          product: FactoryGirl.attributes_for(:product, price: -1)
        expect(assigns(:result)).to eq(false)
      end

      it 'assigns @product with errors' do
        xhr :put, :update, id: @product.id ,
          product: FactoryGirl.attributes_for(:product, price: -1)
        expect(assigns(:product).errors.empty?).to eq(false)
      end
    end
  end

  # DESTROY
  describe 'DELETE destroy' do

    before :each do
      @product = FactoryGirl.create(:product) # Creates and saves valid object
    end

    it 'destroys the requested product' do
      expect {
        delete :destroy, id: @product.id
      }.to change(Product, :count).by(-1)
    end

    it 'redirects to the products list' do
      delete :destroy, id: @product.id
      expect(response).to redirect_to(products_url)
    end
  end
end