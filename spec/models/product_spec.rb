require 'rails_helper'
require 'faker'

describe Product do

  it 'has a valid factory' do
    expect(create(:product)).to be_valid
  end
  
  context 'with a valid product name' do
    
    it 'requires a product name' do
      expect(build(:product, name: nil)).to_not be_valid
    end
    
    it 'raises error on no name given' do
      product = build(:product, name: nil)
      product.valid?
      expect(product.errors.messages[:name]).to_not be_empty
    end
    
    it 'does not allow product names longer than 40 characters' do
      expect(build(:product, name: Faker::Lorem.characters(41))).not_to be_valid
    end
    
    it 'raises error when given name is more than 40 characters' do
      product = build(:product, name: Faker::Lorem.characters(41))
      product.valid?
      expect(product.errors.messages[:name]).to_not be_empty
    end
  end
  
  context 'with a valid price' do
  
    it 'requires a price' do
      expect(build(:product, price: nil)).to_not be_valid
    end
    
    it 'raises error when no price is given' do
      product = build(:product, price: nil)
      product.valid?
      expect(product.errors.messages[:price]).to_not be_empty
    end
    
    it 'does not allow negative prices' do
      expect(build(:product, price: -1)).to_not be_valid
    end
    
    it 'raises error when negative price is given' do
      product = build(:product, price: -1)
      product.valid?
      expect(product.errors.messages[:price]).to_not be_empty
    end
    
    describe 'rounds' do
      before {@product = build(:product)}
      
      it 'up values greater or equal to .005' do
        @product.price = 3.145
        expect(@product.price).to eq(3.15)
      end
      
      it 'down values smaller than .005' do
        @product.price = 3.144
        expect(@product.price).to eq(3.14)
      end
    end
  end
  
  context 'with a valid description' do
    
    it 'requires a description' do
      expect(build(:product, description: nil)).to_not be_valid
    end
    
    it 'raises error when no description is given' do
      product = build(:product, description: nil)
      product.valid?
      expect(product.errors.messages[:description]).to_not be_empty
    end
    
    it 'does not allow a description longer than 255 characters' do
      expect(build(:product, description: Faker::Lorem.characters(256))).to_not be_valid
    end
    
    it 'raises error when description given is longer than 255 characters' do
      product = build(:product, description: nil)
      product.valid?
      expect(product.errors.messages[:description]).to_not be_empty
    end
  end

end
