require "rails_helper"

# Feature: Update A Product
#   As a visitor
#   I want to add a update a product in the listing
feature 'Update Product' do

  before :each do
    page.driver.browser.manage.window.maximize
    @product = FactoryGirl.create(:product)
    visit root_path
    click_link("product-#{@product.id}")
    click_link("Edit")
  end

  # Scenario: User can navigate to update product page
  #   Given I am a user
  #   I click on a product in the listing
  #   I click on the 'Edit' button
  #   I should see a form for updating the product, with existing fields already filled
  scenario "user can navigate to update product page", js: true do

    expect(find("#new-modal")).to be_visible

    # Expect modal to be visible
    expect(page).to have_css("#edit_product_#{@product.id}")

    # Expect form to be populated
    within(:css, '#new-modal') do
      expect(page).to have_xpath("//input[@value='#{@product.name}']")
      expect(page).to have_xpath("//input[@value='#{@product.price}']")
      expect(page).to have_content(@product.description)
      expect(page).to have_selector(:link_or_button, 'Save')
    end

  end

  # Scenario: User can update a product with valid attributes
  #   Given I am a user
  #   I am in the edit product modal
  #   I perform valid changes
  #   I should see a the updated product
  scenario "user can update a product with valid attributes", js: true do

  # Fill in valid information
    fill_in 'Name', with: "A Edited Product"
    fill_in 'Price', with: "12.425"
    fill_in 'Description', with: "Let me put a valid description."

    click_button('Save')
    expect(page).to have_selector('#new-modal', visible: false)

    expect(page).to have_content("A Edited Product")
    expect(page).to have_content("$12.43")
    expect(page).to have_content("Let me put a valid description.")

  end

  # Scenario: User cannot update a product with no product name
  #   Given I am a user
  #   I am in the edit product modal
  #   I fill in the form without a name
  #   I should see an error message
  scenario "user cannot update a product with no product name", js: true do

    fill_in 'Name', with: ""
    fill_in 'Price', with: "12.425"
    fill_in 'Description', with: "Let me put a valid description."

    click_button('Save')
    expect(page).to have_content('Name must be present.')
  end

  # Scenario: User cannot update a product with a very long name
  #   Given I am a user
  #   I am in the edit product modal
  #   I change the product name to an extremely long name
  #   I should see an error message
  scenario "user cannot update a product with a very long name", js: true do

    fill_in 'Name', with:
      "A very long test product. A very long test product. A very long test product. A very long test product."
    fill_in 'Price', with: "12.43"
    fill_in 'Description', with: "Let me put a valid description."

    click_button('Save')
    expect(page).to have_content('Name cannot be more than 40 characters long.')
  end

  # Scenario: User cannot update a product without a price
  #   Given I am a user
  #   I am in the edit product modal
  #   I fill in the form without a price
  #   I should see an error message
  scenario "user cannot update a product without a price", js: true do

    fill_in 'Price', with: ""
    fill_in 'Name', with: "A Test Product"
    fill_in 'Description', with: "Let me put a valid description."

    click_button('Save')
    expect(page).to have_content('Price is not a number')
  end

  # Scenario: User cannot update a product without a description
  #   Given I am a user
  #   I am in the edit product modal
  #   I fill in the form without a description
  #   I should see an error message
  scenario "user cannot update a product without a description", js: true do

    fill_in 'Name', with: "A Test Product"
    fill_in 'Price', with: "12.425"
    fill_in 'Description', with: ""

    click_button('Save')
    expect(page).to have_content('Description must be present.')
  end

  # Scenario: User cannot update a product with a very long description
  #   Given I am a user
  #   I am in the edit product modal
  #   I fill in the form with a very long description
  #   I should see an error message
  scenario "user cannot update a product with a very long description", js: true do

    fill_in 'Name', with: "A Test Product"
    fill_in 'Price', with: "12.425"
    fill_in 'Description', with: "This is a long description.
      This is a long description.
      This is a long description. This is a long description.
      This is a long description. This is a long description.
      This is a long description. This is a long description.
      This is a long description. This is a long description."

    click_button('Save')
    expect(page).to have_content('Description cannot be more than 255 characters long.')
  end
end