require "rails_helper"

# Feature: Delete Product
#   As a visitor
#   I want to be able to delete products
feature 'Delete Product', js:true do

  before { @product = FactoryGirl.create(:product) }

  # Scenario: User can delete after selecting a product from the listing
  #   I am a user
  #   I select an item from the product listing
  #   I click on the Delete button
  #   I confirm the pop up dialog
  #   I should not see the product in the listing
  scenario 'user can delete after selecting a product from the listing' do
    visit root_path
    click_link("product-#{@product.id}")

    # Click on delete and accept
    click_link("Delete")
    page.driver.browser.switch_to.alert.accept

    # Product should be gone
    expect(page).to_not have_content @product.name
  end

  # Scenario: User can delete after selecting a product from the listing
  #   I am a user
  #   I select an item from the product listing
  #   I click on the Delete button
  #   I decline the pop up dialog
  #   I should still see the product in the listing
  scenario 'user should not delete a product if they did not confirm the dialog' do
    visit root_path
    click_link("product-#{@product.id}")

    # Click on delete and accept
    click_link("Delete")
    page.driver.browser.switch_to.alert.dismiss

    # Product should be gone
    expect(page).to have_content @product.name
  end
end