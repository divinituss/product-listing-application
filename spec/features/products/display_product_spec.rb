require "rails_helper"

# Feature: Display Products
#   As a visitor
#   I want to see the products available on a website
#   I want to be able to view the details of a product by clicking on it
feature 'Display Products' do
  
  # Create and save some valid products
  before :each do
    @product = FactoryGirl.create(:product)
    @product2 = FactoryGirl.create(:product)
  end
  
  # Scenario: Visit the home page
  # I visit the home page
  # I see the products being listed
  scenario "visit home page" do
    visit root_path
    expect(page).to have_content @product.name
    expect(page).to have_content @product.price
    expect(page).to have_content @product2.name
    expect(page).to have_content @product2.price
  end
  
  # Scenario: Click on product link
  # I visit the homepage
  # I click on a product in the listing
  # I see the details of the product
  scenario "click on product link", :js => true do
    visit root_path
    click_link("product-#{@product.id}")
    expect(page).to have_content @product.description
  end
end
