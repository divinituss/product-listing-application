require "rails_helper"

# Feature: Create New Product
#   As a visitor
#   I want to add a new product to the listing
feature 'Create New Product' do

  before :each do
    page.driver.browser.manage.window.maximize
    visit root_path
    click_link('New Product')
  end

  # Scenario: User can navigate to create product page
  #   Given I am a user
  #   I visit the home page
  #   I click on 'New Product' button
  #   I should see a form for creating a new product
  scenario "user can navigate to create product page", js: true do

    expect(find("#new-modal")).to be_visible

    # Expect form to be populated
    expect(page).to have_css("#new_product")
    expect(page).to have_content("Name")
    expect(page).to have_content("Price")
    expect(page).to have_content("Description")
    expect(page).to have_selector(:link_or_button, 'Save')
  end

  # Scenario: User can create a product with valid attributes
  #   Given I am a user
  #   I visit the home page and click on 'New Product'
  #   I fill in a valid product
  #   I should see a new product created
  scenario "user can create a product with valid attributes", js: true do
  # visit root_path
  # click_link('New Product')

  # Fill in valid information
    fill_in 'Name', with: "A Test Product"
    fill_in 'Price', with: "12.425"
    fill_in 'Description', with: "Let me put a valid description."

    click_button('Save')
    expect(page).to have_selector('#new-modal', visible: false)
    within(:css, '#page-content-wrapper') do
      expect(page).to have_content("A Test Product")
      expect(page).to have_content("$12.43")
      expect(page).to have_content("Let me put a valid description.")
    end
  end

  # Scenario: User cannot create a product with no product name
  #   Given I am a user
  #   I visit the home page and click on 'New Product'
  #   I fill in the new product form without a name
  #   I should see an error message
  scenario "user cannot create a product with no product name", js: true do
  # visit root_path
  # click_link('New Product')

    fill_in 'Price', with: "12.425"
    fill_in 'Description', with: "Let me put a valid description."

    click_button('Save')
    expect(page).to have_content('Name must be present.')
  end

  # Scenario: User cannot create a product with a very long name
  #   Given I am a user
  #   I visit the home page and click on 'New Product'
  #   I fill in the new product form with an extremely long name
  #   I should see an error message
  scenario "user cannot create a product with a very long name", js: true do

    fill_in 'Name', with: "A very long test product.
    A very long test product. A very long test product.
    A very long test product. A very long test product.
    A very long test product. A very long test product. "
    fill_in 'Price', with: "12.425"
    fill_in 'Description', with: "Let me put a valid description."

    click_button('Save')
    expect(page).to have_content('Name cannot be more than 40 characters long.')
  end

  # Scenario: User cannot create a product without a price
  #   Given I am a user
  #   I visit the home page and click on 'New Product'
  #   I fill in the new product form without a price
  #   I should see an error message
  scenario "user cannot create a product without a price", js: true do

    fill_in 'Name', with: "A Test Product"
    fill_in 'Description', with: "Let me put a valid description."

    click_button('Save')
    expect(page).to have_content('Price is not a number')
  end

  # Scenario: User cannot create a product without a description
  #   Given I am a user
  #   I visit the home page and click on 'New Product'
  #   I fill in the new product form without a description
  #   I should see an error message
  scenario "user cannot create a product without a description", js: true do

    fill_in 'Name', with: "A Test Product"
    fill_in 'Price', with: "12.425"

    click_button('Save')
    expect(page).to have_content('Description must be present.')
  end

  # Scenario: User cannot create a product with a very long description
  #   Given I am a user
  #   I visit the home page and click on 'New Product'
  #   I fill in the new product form with a very long description
  #   I should see an error message
  scenario "user cannot create a product with a very long description", js: true do

    fill_in 'Name', with: "A Test Product"
    fill_in 'Price', with: "12.425"
    fill_in 'Description', with:
      "This is a long description. This is a long description.
      This is a long description. This is a long description.
      This is a long description. This is a long description.
      This is a long description. This is a long description.
      This is a long description. This is a long description.
      This is a long description. This is a long description."

    click_button('Save')
    expect(page).to have_content('Description cannot be more than 255 characters long.')
  end
end