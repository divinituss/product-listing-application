require 'rails_helper'

describe "Products" do
  
  # Test if rails recognizes products_path
  describe "GET /products" do
    it "directs to /products using 'products_path'" do
      get products_path
      response.status.should be(200)
    end
  end
end
