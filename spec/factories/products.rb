require 'faker'

FactoryGirl.define do
  factory :product do |p|
    p.name { Faker::Commerce.product_name }
    p.price { 1420.35 }
    p.description { Faker::Lorem.characters } # defaulted at 255 chars
  end
end