class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]

  # GET /products
  def index
    @products = Product.all.order('products.created_at DESC')
  end

  # GET /products/1
  def show
    respond_to do |format|
      format.js
      format.all {render_404}
    end
  end

  # GET /products/new, type JS
  def new
    @product = Product.new
    respond_to do |format|
      format.js
      format.all {render_404}
    end
  end

  # GET /products/1/edit
  def edit
    # Handled in :set_product before_action
  end

  # POST /products, type JS
  def create
    @product = Product.new(product_params)

    respond_to do |format|
    # Handling of result is done in create.js
      @result = @product.save
      if @result
        format.js {redirect_to @product}
      else
        format.js
      end
      format.all {render_404}
    end
  end

  # PATCH/PUT /products/1, type JS
  def update
    respond_to do |format|
      @result = @product.update(product_params)
      if @result
        format.js {redirect_to @product}
      else
        format.js
      end
      format.all {render_404}
    end
  end

  # DELETE /products/1
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  # Called in before_action
  def set_product
    @product = Product.find(params[:id])
  end

  # Strong parameters (Rails 4+)
  def product_params
    params.require(:product).permit(:name, :price, :description)
  end
  
  # Renders a 404 page
  def render_404
    raise ActionController::RoutingError.new('Not Found')
  end
end
