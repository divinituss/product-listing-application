class Product < ActiveRecord::Base

  # Make sure price is positive
  monetize :price_cents, numericality: {
    greater_than_or_equal_to: 0
  }

  validates :name,
    presence: {message: "must be present."},
    length: { maximum: 40, message: "cannot be more than 40 characters long." }
    
  validates :description,
    presence: { message: "must be present." },
    length: { maximum: 255, message: "cannot be more than 255 characters long." }
    
  validates :price, presence: { message: "must be present." }

end
