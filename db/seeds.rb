# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

require 'faker'

Product.delete_all

Product.create! [
  { name: Faker::Commerce.product_name, price: 123.12, description: Faker::Lorem.characters },
  { name: Faker::Commerce.product_name, price: 234, description: Faker::Lorem.characters },
  { name: Faker::Commerce.product_name, price: 54.2, description: Faker::Lorem.characters },
  { name: Faker::Commerce.product_name, price: 30, description: Faker::Lorem.characters },
  { name: Faker::Commerce.product_name, price: 354.2, description: Faker::Lorem.characters },
  { name: Faker::Commerce.product_name, price: 6564.12, description: Faker::Lorem.characters },
  { name: Faker::Commerce.product_name, price: 98123.1, description: Faker::Lorem.characters },
  { name: Faker::Commerce.product_name, price: 999999999, description: Faker::Lorem.characters },
  { name: Faker::Commerce.product_name, price: 9792479299, description: Faker::Lorem.characters }
]
