class AddPriceCentsToProducts < ActiveRecord::Migration
  def change
    add_column :products, :price_cents, :integer, :limit => 8
  end
end